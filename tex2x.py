import sys, os

sys.path.append('./src')

import plasTeX
from plasTeX.TeX import TeX
from plasTeX.ConfigManager import *
from plasTeX.Renderers.HTML5 import Renderer
from renderers.MyRenderer import Renderer as MyRenderer
from plasTeX.Config import config
from plasTeX.Renderers.HTML5.Config import config as html5config
#from renderers.MyRenderer import Renderer

def handle_myBold(node):
	print('node attributes', node.attributes)
	print('node as XML', node.toXML())
	print('node name', node.nodeName)
	print('node type', node.nodeType)
	# print('node parent', node.parentNode)
	print('node children', node.childNodes)
	print('node textcontent', node.textContent)
	return 'xxx %s' % node.attributes['text']

def main():
	#os.chdir('./public')
	c = config
	c += html5config
	# c = ConfigManager()
	# c += config
	# c += html5config
	#c['html5']['localtoc-level'] = 4

	renderer = MyRenderer()
	renderer['myBold'] = handle_myBold
	renderer['lol'] = handle_myBold

	renderer['extra-css'] = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'public')
	c['document']['toc-depth'] = 10
	c['general']['theme'] = 'inspinia'
	c['general']['use-css'] = ['bootstrap.min.css', 'custom.css']
	c['general']['use-js'] = ['jquery.min.js', 'bootstrap.min.js']
	c['files']['directory'] = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'public')
	print('config', c['html5']['localtoc-level'])
	document = plasTeX.TeXDocument()
	document.userdata['working-dir'] = os.getcwd()

	tex = TeX(document, myfile='module.tex')
	tex.parse()
	Renderer.render(document)

if __name__ == "__main__":
    main()

# def main(fname):
#     document = plasTeX.TeXDocument()
#     tex = TeX(document, myfile=fname)
#     tex.parse()
#     Renderer().render(document)
#
# if __name__ == '__main__':
#     fname = 'sample2e.tex'
#     main(fname)
# ~
