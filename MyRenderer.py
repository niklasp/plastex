import string
from plasTeX.Renderers.HTML5 import Renderer as HTML5Renderer

class Renderer(HTML5Renderer):

    def __init__(self):
        super(Renderer, self).__init__()
        self['lol'] = self.handle_lol

    def loadTemplates(self, document):
        print('hello from MyRenderer YOOyOOOLO')
        super(Renderer, self).loadTemplates(document)

    def handle_lol(self, node):
        print('XXXXX X X X XX X X ', node.nodeName, node.attributes, node.textContent, node)
        if node.hasAttributes():
            print ('node has attributes')
        else:
            print ('noooooo attributes')
        return u"was geht"

    def default(self, node):
        print(node)
        return super(Renderer, self).default(node)

    def textDefault(self, node):
        """ Rendering method for all text nodes """
        return node.replace('&','&amp;').replace('<','&lt;').replace('>','&gt;')

Renderer = Renderer
