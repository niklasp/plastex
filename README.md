# Testing Repo for Plastex

Try the following:

### plastex
1. run `plastex module.tex` or `plastex testing.tex` from the command line
2. check the `module/index.html` or `testing/index.html`

### python
1. run `python3 render.py` from the commandline
2. check `dist/index.html`