from plasTeX import Base
from plasTeX.PackageResource import (
	PackageResource, PackageCss, PackageJs, PackageTemplateDir)


# Also, das läuft offiziell so ab mit den packages:

# zuerst ist zu unterscheiden zwischen LaTeX und python

# A. LaTeX wird gerendered

# 1. hierbei passieren viele dinge, u.a. wird kontrolliert ob die LaTeX Befehle
# auch übersetzt werden können

# 2. ....

# B. dann wird der Renderer aufgerufen

# 1. Wenn ein LaTeX Befehl \usepackage{packageX} gefunden wird, wird
#     Context.loadPackage aufgerufen
# 2. Das wiederum sucht nach packageX.py
# 3. In packageX.py können alle Befehle des packages definiert werden, sowie
# zusätzliche ressourcen die für das paket benötigt werden (css / js) definiert werden,
# die dann vom HTML5 renderer an die richtige stelle im output directory kopiert werden können

class myBold(Base.Command):
	args = 'text:str'
	def invoke(self, tex):
		Base.Command.invoke(self, tex)

class testcommand(Base.Command):
    """ 
    belongs to the LaTeX command
    \testcommand[2]{green}
    with two optional arguments width and pos and one mandatory argument, text
    """
    args = 'color:str'
    def invoke(self, tex):
        Base.Command.invoke(self, tex)

class lol(Base.Command):
	args = '[ width:int ] [ height:int ] text:str'
	def invoke(self, tex):
		Base.Command.invoke(self, tex)

class MExercise(Base.Command):
	args = 'text:str'
	def invoke(self, tex):
		Base.Command.invoke(self, tex)


def ProcessOptions(options, document):
    css = PackageCss(
        renderers='html5',
        data='test.css',
        package='userpackage')
    js = PackageJs(
        renderers='html5',
        data='userscript.js',
        package='userpackage')
    tpl = PackageTemplateDir(
        renderers='html5',
        package='userpackage')

    document.addPackageResource([css, js, tpl])