/*****
* CONFIGURATION
*/
    //Main navigation
    $.navigation = $('nav > ul.nav');

  $.panelIconOpened = 'icon-arrow-up';
  $.panelIconClosed = 'icon-arrow-down';

  //Default colours
  $.brandPrimary =  '#20a8d8';
  $.brandSuccess =  '#4dbd74';
  $.brandInfo =     '#63c2de';
  $.brandWarning =  '#f8cb00';
  $.brandDanger =   '#f86c6b';

  $.grayDark =      '#2a2c36';
  $.gray =          '#55595c';
  $.grayLight =     '#818a91';
  $.grayLighter =   '#d1d4d7';
  $.grayLightest =  '#f8f9fa';

'use strict';

$.fn.hasParent=function(e){
  return !!$(this).parents(e).length
}

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){

  var links = {};
  links.next =  $( "link[rel='next']" ).prop( "href" );
  links.prev =  $( "link[rel='prev']" ).prop( "href" );
  links.up   =  $( "link[rel='up']" ).prop( "href" );




  // set up keyboard arrow navigation to next / prev chapters
  // online on mobile (i.e. touch enabled devices)

  function isMobile() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false; }
  }
  
  if (isMobile()) {
    var mainElement = document.getElementById('main');
    
      // create a simple instance
      // by default, it only adds horizontal recognizers
      var mc = new Hammer(mainElement, {threshold: 50});
    
      // listen to events...
      mc.on("panleft", function(ev) {
        if (typeof links.prev !== "undefined") {
          
          //return if we slide over a mathjax element, they need to be scrolled
          if ($(ev.target).hasParent('.displaymath, .equation')) {
            return false;
          }
          console.log('going to previous chapter');
          location.href = links.prev;
        }
      });
    
      mc.on("panright", function(ev) {
        if (typeof links.next !== "undefined") {
          
          if ($(ev.target).hasParent('.displaymath, .equation')) {
            return false;
          }
          console.log('going to next chapter');
          location.href = links.next;
        }
      });
  }
  

  // set up keyboard arrow navigation to next / prev chapters
  $(document).keydown(function(e){
      if (e.keyCode == 37 && typeof links.prev !== "undefined") {  // left
        console.log('going to previous chapter');
        location.href = links.prev;
        return false;
      } else if (e.keyCode == 39 && typeof links.next !== "undefined") {  // right
        console.log('going to next chapter');
        location.href = links.next;
        return false;
      } else if (e.keyCode == 38 && typeof links.up !== "undefined") {
        console.log('going one chapter up');
        location.href = links.up;
      }
  });


  
    

  // Add class .active to current link
  $.navigation.find('a').each(function(){

    var cUrl = String(window.location).split('?')[0];

    if (cUrl.substr(cUrl.length - 1) == '#') {
      cUrl = cUrl.slice(0,-1);
    }

    if ($($(this))[0].href==cUrl) {
      $(this).addClass('active');

      $(this).parents('ul').add(this).each(function(){
        $(this).parent().addClass('open');
      });
    }
  });

  // Dropdown Menu
  $.navigation.on('click', 'a', function(e){

    if ($.ajaxLoad) {
      e.preventDefault();
    }

    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }

  });

  function resizeBroadcast() {

    var timesRun = 0;
    var interval = setInterval(function(){
      timesRun += 1;
      if(timesRun === 5){
        clearInterval(interval);
      }
      window.dispatchEvent(new Event('resize'));
    }, 62.5);
  }

  /* ---------- Main Menu Open/Close, Min/Full ---------- */
  $('.sidebar-toggler').click(function(){
    $('body').toggleClass('sidebar-hidden');
    resizeBroadcast();
  });

  $('.sidebar-minimizer').click(function(){
    $('body').toggleClass('sidebar-minimized');
    resizeBroadcast();
  });

  $('.brand-minimizer').click(function(){
    $('body').toggleClass('brand-minimized');
  });

  $('.aside-menu-toggler').click(function(){
    $('body').toggleClass('aside-menu-hidden');
    resizeBroadcast();
  });

  $('.mobile-sidebar-toggler').click(function(){
    $('body').toggleClass('sidebar-mobile-show');
    resizeBroadcast();
  });

  $('.sidebar-close').click(function(){
    $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
  });

  /* ---------- Disable moving to top ---------- */
  $('a[href="#"][data-top!=true]').click(function(e){
    e.preventDefault();
  });

});

/****
* CARDS ACTIONS
*/

$(document).on('click', '.card-actions a', function(e){
  e.preventDefault();

  if ($(this).hasClass('btn-close')) {
    $(this).parent().parent().parent().fadeOut();
  } else if ($(this).hasClass('btn-minimize')) {
    var $target = $(this).parent().parent().next('.card-block');
    if (!$(this).hasClass('collapsed')) {
      $('i',$(this)).removeClass($.panelIconOpened).addClass($.panelIconClosed);
    } else {
      $('i',$(this)).removeClass($.panelIconClosed).addClass($.panelIconOpened);
    }

  } else if ($(this).hasClass('btn-setting')) {
    $('#myModal').modal('show');
  }

});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function init(url) {

  /* ---------- Tooltip ---------- */
  $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

  /* ---------- Popover ---------- */
  $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();

}
