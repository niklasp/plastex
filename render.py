import os,sys, subprocess
from plasTeX.TeX import TeX
from plasTeX import TeXDocument
from MyRenderer import Renderer
from plasTeX.Config import config
from plasTeX.Renderers.HTML5.Config import config as html5_config


from commands import *

# build a html5 version

# create a document class with the correct configs
document = TeXDocument(config=config+html5_config)
document.userdata['working-dir'] = os.getcwd()


# Instantiate a TeX processor and parse the input text
tex = TeX(document, myfile='testing.tex')
tex.loadPackage('./tex/userpackage.sty')
tex.parse()

# dont know yet if we better use this or the other config options
tex.ownerDocument.userdata['working-dir'] = 'dist'
tex.ownerDocument.config['general']['theme'] = 'coreUI'
tex.ownerDocument.config['html5']['mathjax-dollars'] = True
tex.ownerDocument.config['html5']['mathjax-url'] = "//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS_CHTML"
tex.ownerDocument.config['html5']['breadcrumbs-level'] = 0

os.chdir(tex.ownerDocument.userdata['working-dir'])

renderer = Renderer()
renderer.render(document)


def generate_pdf():

    current_dir = os.path.dirname(os.path.realpath(__file__))
    sourceTEX = current_dir
    sourcePDFStartFile = os.path.join(current_dir, 'testing.tex')

    if False:
        print( "PDF generation not activated in options")
        return
    
    
    os.chdir(sourceTEX)
    pdfok = True
    print("Generating PDF file from source " + sourcePDFStartFile)
    for cl in [ ["pdflatex", "-halt-on-error", "-interaction=errorstopmode", sourcePDFStartFile], \
                ["pdflatex", "-halt-on-error", "-interaction=errorstopmode", sourcePDFStartFile], \
                ["makeindex", "-q", 'testing'], \
                ["pdflatex", "-halt-on-error", "-interaction=errorstopmode", sourcePDFStartFile]]:
        if pdfok:
            p = subprocess.Popen(cl, stdout = subprocess.PIPE, shell = False, universal_newlines = True, cwd=current_dir)
            (output, err) = p.communicate()
            if p.returncode < 0:
                print( "Call to " + cl[0] + " for file " + sourcePDFStartFile + " was terminated by a signal (POSIX return code " + p.returncode + ")")
                pdfok = False
            else:
                if p.returncode > 0:
                    print( cl[0] + " could not process file " + sourcePDFStartFile + ", error lines have been written to logfile")
                    s = output[-256:]
                    s = s.replace("\n",", ")
                    print( "Last " + cl[0] + " lines: " + s)
                    pdfok = False
                else:
                    print(cl[0] + " finished successfully")

        else:
            print( "Skipping system call to " + cl[0] + " on " + sourcePDFStartFile + " due to previous tex errors")

    if pdfok:
        print("Generation of PDF file for " + sourcePDFStartFile + " successfull")
        target = 'testing' + ".pdf"
        #self.sys.copyFile("." , self.options.targetpath, target)
        print( "Generated " + target + " in top level directory")
    else:
        print("PDF generation aborted for " + sourcePDFStartFile)


generate_pdf()